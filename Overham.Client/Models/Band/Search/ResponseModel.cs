﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Overham.Client.Models.Band.Search
{
    public class ResponseModel
    {
        public int[] Data { get; set; }
        public bool Error { get; set; }
    }
}
