﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Overham.Client.Models.Band.Lookup
{
    public class DataModel
    {
        public string Name { get; set; }
        public string Country { get; set; }
        public string Id { get; set; }
        public string Lower { get; set; }
        public string Upper { get; set; }
    }
}
