﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Overham.Client.Models.Resistor.Encode
{
    public class DataModel
    {
        public string Band1str { get; set; }
        public string Band0 { get; set; }
        public string Band2str { get; set; }
        public string Band0str { get; set; }
        public string Band1 { get; set; }
        public string Band2 { get; set; }
    }
}
