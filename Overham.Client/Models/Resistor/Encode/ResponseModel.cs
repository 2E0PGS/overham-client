﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Overham.Client.Models.Resistor.Encode
{
    public class ResponseModel
    {
        public DataModel Data { get; set; }
        public MapModel Map { get; set; }
        public bool ExitCode { get; set; }
    }
}
