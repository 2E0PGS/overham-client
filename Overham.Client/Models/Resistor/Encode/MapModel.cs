﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Overham.Client.Models.Resistor.Encode
{
    public class MapModel
    {
        [JsonProperty(PropertyName ="0")]
        public string Band0 {get;set;}
        [JsonProperty(PropertyName = "1")]
        public string Band1 { get; set; }
        [JsonProperty(PropertyName = "2")]
        public string Band2 { get; set; }
        [JsonProperty(PropertyName = "3")]
        public string Band3 { get; set; }
        [JsonProperty(PropertyName = "4")]
        public string Band4 { get; set; }
        [JsonProperty(PropertyName = "5")]
        public string Band5 { get; set; }
        [JsonProperty(PropertyName = "6")]
        public string Band6 { get; set; }
        [JsonProperty(PropertyName = "7")]
        public string Band7 { get; set; }
        [JsonProperty(PropertyName = "8")]
        public string Band8 { get; set; }
        [JsonProperty(PropertyName = "9")]
        public string Band9 { get; set; }
        [JsonProperty(PropertyName = "10")]
        public string Band10 { get; set; }
        [JsonProperty(PropertyName = "11")]
        public string Band11 { get; set; }
    }
}
