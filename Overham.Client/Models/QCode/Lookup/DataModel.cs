﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Overham.Client.Models.QCode.Lookup
{
    public class DataModel
    {
        public string Tx { get; set; }
        public string Rx { get; set; }
        public string Authority { get; set; }
    }
}
