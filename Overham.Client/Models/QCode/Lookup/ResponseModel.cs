﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Overham.Client.Models.QCode.Lookup
{
    public class ResponseModel
    {
        public DataModel Data { get; set; }
        public bool Error { get; set; }
    }
}
