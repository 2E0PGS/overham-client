﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Overham.Client.Models.Recordings.Search
{
    public class DataModel
    {
        public int ResultCount { get; set; }
        public List<ResultsModel> Results { get; set; }
    }
}
