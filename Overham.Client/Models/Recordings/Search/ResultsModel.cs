﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Overham.Client.Models.Recordings.Search
{
    public class ResultsModel
    {
        public string Duration { get; set; }
        [JsonProperty (PropertyName = "hzFreq")]
        public string HZFreq { get; set; }
        public string Id { get; set; }
        public DateTime WhenStart { get; set; }
    }
}
