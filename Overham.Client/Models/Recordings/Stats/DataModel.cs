﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Overham.Client.Models.Recordings.Stats
{
    public class DataModel
    {
        public string UploadedCount { get; set; }
        public DateTime LastDump { get; set; }
        public string Available { get; set; }
        public string TotalCount { get; set; }
        public DateTime Earliest { get; set; }
        public DateTime Latest { get; set; }
        [JsonProperty (PropertyName = "mode:am")]
        public string ModeAmCount { get; set; }
        [JsonProperty(PropertyName = "mode:fm")]
        public string ModeFmCount { get; set; }
        [JsonProperty(PropertyName = "mode:lsb")]
        public string ModeLsbCount { get; set; }
        [JsonProperty(PropertyName = "mode:usb")]
        public string ModeUsbCount { get; set; }
        [JsonProperty(PropertyName = "mode:dv")]
        public string ModeDVCount { get; set; }
        [JsonProperty(PropertyName = "mode:cw")]
        public string ModeCWCount { get; set; }
    }
}
