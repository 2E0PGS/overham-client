﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Overham.Client.Models.Recordings.Info
{
    public class DataModel
    {
        public string Mode { get; set; }
        public DateTime WhenStart { get; set; }
        public int Length { get; set; }
        public string Callsign { get; set; }
        [JsonProperty(PropertyName = "hzFreq")]
        public int HZFreq { get; set; }
        public string Model { get; set; }
        public int Band { get; set; }
        public string Maidenhead { get; set; }
        public string Direction { get; set; }
    }
}
