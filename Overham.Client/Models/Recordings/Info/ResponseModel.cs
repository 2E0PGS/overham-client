﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Overham.Client.Models.Recordings.Info
{
    public class ResponseModel
    {
        public DataModel Data { get; set; }
        public bool Error { get; set; }
    }
}
