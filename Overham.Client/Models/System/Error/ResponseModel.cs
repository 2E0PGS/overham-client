﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Overham.Client.Models.System.Error
{
    public class ResponseModel
    {
        public DataModel Data { get; set; }
        public bool Error { get; set; }
    }
}
