﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Overham.Client.Models.System.Error
{
    public class DataModel
    {
        public string Symbolic { get; set; }
        public string Message { get; set; }
        public string Id { get; set; }
    }
}
