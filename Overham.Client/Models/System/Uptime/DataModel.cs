﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Overham.Client.Models.System.Uptime
{
    public class DataModel
    {
        public int Seconds { get; set; }
    }
}
