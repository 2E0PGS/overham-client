﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Overham.Client.Models.System.Ping
{
    public class ResponseModel
    {
        public string Data { get; set; }
        public bool Error { get; set; }
    }
}
