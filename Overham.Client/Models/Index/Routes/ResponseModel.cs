﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Overham.Client.Models.Index.Routes
{
    public class ResponseModel
    {
        public List<string> Data { get; set; }
        public bool Error { get; set; }
    }
}
