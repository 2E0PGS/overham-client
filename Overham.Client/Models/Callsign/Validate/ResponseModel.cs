﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Overham.Client.Models.Callsign.Validate
{
    public class ResponseModel
    {
        public bool Data { get; set; }
        public bool Error { get; set; }
    }
}
