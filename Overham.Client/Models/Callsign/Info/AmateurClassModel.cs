﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Overham.Client.Models.Callsign.Info
{
    public class AmateurClassModel
    {
        public int Value { get; set; }
        public string String { get; set; }
    }
}
