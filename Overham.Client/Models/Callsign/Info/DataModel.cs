﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Overham.Client.Models.Callsign.Info
{
    public class DataModel
    {
        public string Region { get; set; }
        public string Country { get; set; }
        public string Value { get; set; }
        public string Valid { get; set; }
        public string Type { get; set; }
        public string Full { get; set; }
        public AmateurClassModel AmateurClass { get; set; }
        public bool Amateur { get; set; }
    }
}
