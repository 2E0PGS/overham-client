﻿using Newtonsoft.Json;
using Overham.Client.Helpers;
using System.Threading.Tasks;

namespace Overham.Client
{
    public class OverhamClient
    {
        public string ApiUrl { get; set; }

        public OverhamClient(string apiUrl)
        {
            ApiUrl = apiUrl;
        }

        public async Task<Models.System.Uptime.ResponseModel> GetSystemUptimeAsync()
        {
            return JsonConvert.DeserializeObject<Models.System.Uptime.ResponseModel>(await HttpHelper.GetAsync(ApiUrl + "/system/uptime"));
        }

        public async Task<Models.System.Ping.ResponseModel> GetSystemPingStatusAsync()
        {
            return JsonConvert.DeserializeObject<Models.System.Ping.ResponseModel>(await HttpHelper.GetAsync(ApiUrl + "/system/ping"));
        }

        public async Task<Models.Index.Routes.ResponseModel> GetIndexRoutesAsync()
        {
            return JsonConvert.DeserializeObject<Models.Index.Routes.ResponseModel>(await HttpHelper.GetAsync(ApiUrl + "/index/routes"));
        }

        public async Task<Models.Callsign.Validate.ResponseModel> GetCallsignValidationAsync(string callsign)
        {
            return JsonConvert.DeserializeObject<Models.Callsign.Validate.ResponseModel>(await HttpHelper.GetAsync(ApiUrl + "/callsign/validate?callsign=" + callsign));
        }

        public async Task<Models.Callsign.Info.ResponseModel> GetCallsignInfoAsync(string callsign)
        {
            return JsonConvert.DeserializeObject<Models.Callsign.Info.ResponseModel>(await HttpHelper.GetAsync(ApiUrl + "/callsign/info?callsign=" + callsign));
        }

        public async Task<Models.QCode.Lookup.ResponseModel> GetQCodeInfoAsync(string qcode)
        {
            return JsonConvert.DeserializeObject<Models.QCode.Lookup.ResponseModel>(await HttpHelper.GetAsync(ApiUrl + "/qcode/lookup?code=" + qcode));
        }

        public async Task<Models.Resistor.Encode.ResponseModel> GetResistorColorBandsAsync(string ohms)
        {
            return JsonConvert.DeserializeObject<Models.Resistor.Encode.ResponseModel>(await HttpHelper.GetAsync(ApiUrl + "/resistor/encode?ohms=" + ohms));
        }

        public async Task<Models.Band.Search.ResponseModel> GetBandByFrequencyAsync(string hz)
        {
            return JsonConvert.DeserializeObject<Models.Band.Search.ResponseModel>(await HttpHelper.GetAsync(ApiUrl + "/band/search?hz=" + hz));
        }

        public async Task<Models.Band.Lookup.ResponseModel> GetBandByIdAsync(string id)
        {
            return JsonConvert.DeserializeObject<Models.Band.Lookup.ResponseModel>(await HttpHelper.GetAsync(ApiUrl + "/band/lookup?id=" + id));
        }

        public async Task<Models.System.Error.ResponseModel> GetErrorInfoAsync(string id)
        {
            return JsonConvert.DeserializeObject<Models.System.Error.ResponseModel>(await HttpHelper.GetAsync(ApiUrl + "/system/error?id=" + id));
        }

        public async Task<Models.Recordings.Stats.ResponseModel> GetRecordingStatsAsync()
        {
            return JsonConvert.DeserializeObject<Models.Recordings.Stats.ResponseModel>(await HttpHelper.GetAsync(ApiUrl + "/recordings/stats"));
        }

        public async Task<Models.Recordings.Search.ResponseModel> GetRecordingsByRangeAsync(string startHZ, string endHZ, string startTime, string endTime)
        {
            return JsonConvert.DeserializeObject<Models.Recordings.Search.ResponseModel>(await HttpHelper.GetAsync(ApiUrl + "/recordings/search?hzFreq=" + startHZ + "-" + endHZ + "&when=" + startTime + "-" + endTime));
        }

        public async Task<Models.Recordings.Search.ResponseModel> GetRecordingsByTimeAndFrequencyAsync(string time, string hz)
        {
            return JsonConvert.DeserializeObject<Models.Recordings.Search.ResponseModel>(await HttpHelper.GetAsync(ApiUrl + "/recordings/search?hzFreq=" + hz + "&when=" + time));
        }

        public async Task<Models.Recordings.Info.ResponseModel> GetRecordingInfoAsync(string id)
        {
            return JsonConvert.DeserializeObject<Models.Recordings.Info.ResponseModel>(await HttpHelper.GetAsync(ApiUrl + "/recordings/info?id=" + id));
        }
    }
}
